import { TemplateController } from 'meteor/template-controller';
import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions';
import { jQuery } from 'meteor/jquery';
import SimpleSchema from 'simpl-schema';
// import template
import './select2.html';
// import npm jquery select2
import select2Init from 'select2/dist/js/select2.full';
// почему то, это работает только так
select2Init(window, jQuery);

checkNpmVersions({
  'simpl-schema': '1.5.3',
}, 'select2');

TemplateController('Select2', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    name: { type: String, optional: true },
    multiple: { type: Boolean, optional: true },
    tags: { type: Boolean, optional: true },
    placeholder: { type: String, optional: true },
    options: { type: Array, defaultValue: [] },
    'options.$': { type: Object, optional: true },
    'options.$.value': { type: String },
    'options.$.label': { type: String },
    width: { type: String, defaultValue: 'resolve' },
    // функции для обработки выбора
    onChange: { type: Function, optional: true },
    onSelect: { type: Function, optional: true },
    onUnselect: { type: Function, optional: true },
    // эти события можно остановить
    onSelecting: { type: Function, optional: true },
    onUnselecting: { type: Function, optional: true },
    // функция для получения селекта
    handleSelect: { type: Function, optional: true },
    selected: { type: SimpleSchema.oneOf(String, Array), optional: true },
    'selected.$': { type: String, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
    this.autorun(() => {
      const {
        // трекает пересоздание селекта
        options,
        tags, placeholder, width, handleSelect, onSelect, onUnselect, onSelecting, onUnselecting,
      } = this.props;
      if (this.select) {
        this.select.select2('destroy');
      }
      Tracker.afterFlush(() => {
        this.select = this.$('select').select2({
          tags,
          placeholder,
          width,
        });
        if (handleSelect) {
          handleSelect(this.select);
        }
        if (onSelect) {
          this.select.off('select2:select').on('select2:select', (e) => {
            onSelect(e.params.data, e);
          });
        }
        if (onUnselect) {
          this.select.off('select2:unselect').on('select2:unselect', (e) => {
            onUnselect(e.params.data, e);
          });
        }
        if (onSelecting) {
          this.select.off('select2:selecting').on('select2:selecting', (e) => {
            onSelecting(e.params.args.data, e);
          });
        }
        if (onUnselecting) {
          this.select.off('select2:unselecting').on('select2:unselecting', (e) => {
            onUnselecting(e.params.args.data, e);
          });
        }
      });
    });
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        name, multiple,
      } = this.props;
      return {
        name,
        multiple,
        class: 'form-control',
      };
    },
    hasPlaceholder() {
      return this.props.placeholder && !this.props.multiple;
    },
    options() {
      return this.props.options.map(({ value, label }) => ({
        atts: { value, selected: this.isSelected(value) },
        label,
      }));
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    // select2 работает над обычным селектом, т.е. обычный селект скрыт под select2
    'change select'(e) {
      if (this.props.onChange) {
        e.preventDefault();
        const select = e.currentTarget;
        this.props.onChange(select.value, select.options[select.selectedIndex].text);
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {
    select: null,
    isSelected(value) {
      if (this.props.selected) {
        if (!Array.isArray(this.props.selected)) {
          return this.props.selected === value;
        }
        return this.props.selected.includes(value);
      }
    },
  },
});
