# Select2

Blaze-Компонент для [select2](https://select2.org/).

Реализует select2 в blaze окружении

## Использование компонента

### Вызов компонета в тимплейте

```spacebars
{{> Select2 selectParams}}
```

### Параметры
```javascript
const props = {
  // имя для select
  name: { type: String, optional: true },
  // множественный выбор
  multiple: { type: Boolean, optional: true },
  // плейсхолдер
  placeholder: { type: String, optional: true },
  // список опций для
  options: { type: Array, defaultValue: [] },
  'options.$': { type: Object, optional: true },
  'options.$.value': { type: String },
  'options.$.label': { type: String },
  // ширина поля select2, по умолчания ширина берется по инпуту
  width: { type: String, defaultValue: 'resolve' },
  // функция для обработки выбора
  onChange: { type: Function, optional: true },
  onSelect: { type: Function, optional: true },
  onUnselect: { type: Function, optional: true },
  // эти события можно остановить
  onSelecting: { type: Function, optional: true },
  onUnselecting: { type: Function, optional: true },
  // функция для получения селекта
  handleSelect: { type: Function, optional: true },
  // ид выбранного параметра
  selectId: { type: String, optional: true },
}
```

### Troubleshooting

#### Некорректная ширина селекта

Проблема возникает если селект2 инициализируется спрятанным *(display:none)*,
передайте необходимую ширину через параметр `width`. Например: `width: 100%`

#### Пересоздание селекта

При обновлении списка опций, селект2 некорректно подтяшивает имя для опции (значение норм),
не понятно либо это проблема реактивности, либо самого селект2.
При пересоздании такой проблемы не возникает.
  

### TODO

- Дополнительные параметры select2
- Добавить Scss стили всесто css, scss коряво собирается
- Тестирование компонента
