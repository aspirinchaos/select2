// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by select2.js.
import { name as packageName } from "meteor/select2";

// Write your tests here!
// Here is an example.
Tinytest.add('select2 - example', function (test) {
  test.equal(packageName, "select2");
});
