Package.describe({
  name: 'select2',
  version: '0.1.4',
  summary: 'Meteor wrapper for select',
  git: 'https://bitbucket.org/aspirinchaos/select2.git',
  documentation: 'README.md',
});

Npm.depends({
  select2: '4.0.6-rc.1',
});

Package.onUse((api) => {
  api.versionsFrom('1.5');
  api.use([
    'ecmascript',
    'tmeasday:check-npm-versions',
    'fourseven:scss',
    'jquery',
    'templating',
    'template-controller',
  ]);
  // sass сборщик метеора, не умеет собирать файлы с одинаковыми названиями
  // см. /scss/_single.scss и /scss/theme/classic/_single.scss
  //api.addFiles('select2.scss', 'client');
  api.addFiles('styles.css', 'client');
  api.mainModule('select2.js', 'client');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('select2');
  api.mainModule('select2-tests.js');
});
